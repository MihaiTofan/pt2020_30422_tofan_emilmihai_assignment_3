package objects;


/**
 * Product class holds all the information from the data in the Product table
 * @author mihai
 *
 */
public class Product 
{
	
	private int idProduct;
	private String name;
	private double price;
	private int quantity;
	
	/**
	 *  An empty constructor for the Product object
	 */
	public Product()
	{
		
	}
	
	/**
	 * Constructor for the Product object which takes a single parameter, the name of the product
	 * @param name represents the string identifier for the name of the Product Object 
	 */
	public Product(String name)
	{
		this.name = name;
	}
	
	/**
	 * Constructor for the Product object, which takes as parameters all the attributes, without the ID of the product
	 * @param name represents the string identifier for the name of the Product Object
	 * @param price represents the double identifier for the price of the Product Object 
	 * @param quantity reprensents the int identifier for the quantity of the Product Object
	 */
	public Product (String name, double price, int quantity)
	{
		this.name = name;
		this.price = price;
		this.quantity = quantity;
	
	}
	
	/**
	 * Getter for the Product's ID
	 * @return returns the int value for the Product's ID
	 */
	public int getIdProduct() 
	{
		return idProduct;
	}
	
	/**
	 * Setter for the Product's ID
	 * @param idProduct represents the int identifier for the Product's ID
	 */
	
	public void setIdProduct(int idProduct)
	{
		this.idProduct = idProduct;
	}
	
	/**
	 * Getter for the Product's name
	 * @return returns the string representing the Product's name
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * Setter for the Product's name
	 * @param name represents the String for the Product's name
	 */
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	/**
	 * Getter for the price of the Object
	 * @return returns the double value representing the Product's price
	 */
	public double getPrice()
	{
		return price;
	}
	
	/**
	 * Setter for the price of the objects 
	 * @param price represents the double value for the Product's price
	 */
	
	public void setPrice(double price)
	{
		this.price = price;
	}
	
	/**
	 * Getter for the Product's quantity
	 * @return returns the int value representing the quantity of the product
	 */
	
	public int getQuantity() 
	{
		return quantity;
	}
	
	/**
	 * Setter for the Product's quantity
	 * @param quantity represents the int value for the Product's quantity
	 */
	public void setQuantity(int quantity) 
	{
		this.quantity = quantity;
	}
	
	/**
	 * Transforms the Product Object into a string and returns the string
	 */
	public String toString()
	{
		String s = " ";
		
		return this.idProduct + s + this.name + s + this.price + s + this.quantity;
	}
	
	
	

}
