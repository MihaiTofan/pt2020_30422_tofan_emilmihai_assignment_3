package objects;

/**
 * Order class holds all the information from the data in the Order table
 * @author mihai
 *
 */
public class Order {

	int idOrder;
	int clientID;
	int productID;
	int quantity;
	
	/**
	 * Empty constructor for the Order object
	 */
	public Order()
	{
		
	}
	
	/**
	 * Constructor for the Client Object which takes as arguments all of the Order's attributes, but without the id
	 * @param clientID represents the int identifier for the Client which placed the order
	 * @param productID represents the int identifier for the Product that was ordered
	 * @param quantity represents the int identifier for the quantity requested
	 */
	public Order(int clientID, int productID, int quantity)
	{
		this.clientID = clientID;
		this.productID = productID;
		this.quantity = quantity ;
	}
	/**
	 * Getter for the Order's ID
	 * @return returns the Order's ID
	 */
	public int getIdOrder()
	{
		return idOrder;
	}
	
	/**
	 * Setter for the Order's ID
	 * @param idOrder represents the int identifier of the Order's ID
	 */
	public void setIdOrder(int idOrder)
	{
		this.idOrder = idOrder;
	}
	
	/**
	 * Getter for the Client's ID
	 * @return returns the int identifier for the Client which placed the order
	 */
	public int getClientID()
	{
		return clientID;
	}
	
	/**
	 * Setter for the Client's ID
	 * @param clientID represents the int identifier for the Client which placed the order
	 */
	public void setClientID(int clientID) 
	{
		this.clientID = clientID;
	}
	
	/**
	 * Getter for the Product's ID
	 * @return returns the int identifier for the requested product's ID
	 */
	public int getProductID()
	{
		return productID;
	}
	
	/**
	 * Setter for the product's ID
	 * @param productID represents the int identifier for the requested Product's ID
	 */
	public void setProductID(int productID) 
	{
		this.productID = productID;
	}
	
	/**
	 * Getter for the quantity that was requested in the order
	 * @return returns the quantity that was requested in the order as an int
	 */
	public int getQuantity()
	{
		return quantity;
	}
	
	/**
	 * Setter for the Order's quantity
	 * @param quantity represents the quantity that was requested in the Order
	 */
	public void setQuantity(int quantity) 
	{
		this.quantity = quantity;
	}
	
	/**
	 * Transforms the Order object into a string and returns the string
	 */
	public String toString()
	{
		String s = " ";
		return this.idOrder + s + this.clientID + s + this.productID + s + this.quantity; 
	}
}
