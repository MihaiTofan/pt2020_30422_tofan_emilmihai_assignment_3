package objects;

/**
* Client class holds all the information from the data in the Client table
* @author mihai
*
*/
public class Client 
{
	private int idClient;
	private String firstName;
	private String lastName;
	private String adress;
	
	/**
	 * An empty constructor for the Client object
	 */
	public Client()
	{
		
	}
	

	
	/**
	 * Constructor for the Client Object which takes as arguments all of the clients attributes, but without the id
	 * @param firstName represents the string identifier for the first name of the Client object
	 * @param lastName represents the string identifier for the last name of the Client object
	 * @param adress represents the string identifier for  the adress of the Client object
	 */
	
	public Client(String firstName, String lastName, String adress)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.adress = adress;
	}
	
	/**
	 * Constructor for the Client Object which creates a new Client Object with a given first and last names
	 * @param firstName represents the string identifier for the first name of the Client object
	 * @param lastName represents the string identifier for the last name of the Client object
	 */
	public Client(String firstName, String lastName)
	{
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	/**
	 * Getter for the ID of the client
	 * @return returns the integer identifier for the Client ID
	 */
	public int getIdClient()
	{
		return idClient;
	}
	
	/**
	 * Setter for the ID of the client
	 * @param idClient represents the integer identifier for the Client ID
	 */
	public void setIdClient(int idClient) 
	{
		this.idClient = idClient;
	}
	
	/**
	 * Getter for the first name of the client
	 * @return returns the string identifier for the first name of the client
	 */
	public String getFirstName() 
	{
		return firstName;
	}
	
	/**
	 * Setter for the first name of the client
	 * @param firstName represents the string identifier for the Client's first name
	 */
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}
	
	/**
	 * Getter for the last name of the client
	 * @return returns the string identifier for the last name of the client
	 */
	public String getLastName() 
	{
		return lastName;
	}
	
	/**
	 * Setter for the last name of the client
	 * @param lastName represents the string identifier for the Client's last name
	 */
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
	
	/**
	 * Setter for the Client's adress
	 * @return returns the string identifier for the Client's adress
	 */
	public String getAdress() 
	{
		return adress;
	}
	
	/**
	 * Setter for the Client's adress
	 * @param adress represents the string identifier for the Client's adress
	 */
	public void setAdress(String adress) 
	{
		this.adress = adress;
	}
	
	/**
	 * Transforms the Client Object into a string and returns the string
	 */
	public String toString()
	{
		String s = " ";
		
		return this.idClient + s + this.firstName + s + this.lastName + s + this.adress;
	}
}