package presentation;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import objects.Client;
import objects.Order;
import objects.Product;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;
import java.util.stream.Stream;

/**
 * The class which generates PDF files, as a result of executing report commands
 * @author mihai
 *
 */
public class PDFGenerator
{
   private Document document;
   
   /**
    * Construcor for the PDFGenerator class which creates a new document
    */
   public PDFGenerator()
   {
	   this.document = new Document();
   }
   
   /**
    * Method for constructing a header for our table
    * @param table represents the table for which this header is constructed
    * @param name represents the name of the table from the DataBase for which we will construct the table header
    */
   private void addTableHeader(PdfPTable table, String name) 
   {
	   
	   switch(name)
	   {
	   case "client":
		   Stream.of("idClient", "firstName", "lastName", "adress")
           .forEach(columnTitle ->
           {
               PdfPCell header = new PdfPCell();
               header.setBackgroundColor(BaseColor.LIGHT_GRAY);
               header.setBorderWidth(2);
               header.setPhrase(new Phrase(columnTitle));
               table.addCell(header);
           });
	   break;
	   
	   case "product":
		   Stream.of("idProduct", "name", "price", "quantity")
           .forEach(columnTitle -> 
           {
               PdfPCell header = new PdfPCell();
               header.setBackgroundColor(BaseColor.LIGHT_GRAY);
               header.setBorderWidth(2);
               header.setPhrase(new Phrase(columnTitle));
               table.addCell(header);
           });
	   break;
	   
	   case "order":
		   Stream.of("idOrder", "clientID", "productID", "quantity")
           .forEach(columnTitle -> 
           {
               PdfPCell header = new PdfPCell();
               header.setBackgroundColor(BaseColor.LIGHT_GRAY);
               header.setBorderWidth(2);
               header.setPhrase(new Phrase(columnTitle));
               table.addCell(header);
           });
	   break;
	   }
		
	}
   
   /**
    * Method for constructing rows from our table
    * @param table represents the table for which the rows are constructed
    * @param name represents the name of the table from the DataBase for which we will construct the table rows
    */
   private void addRows(PdfPTable table, String name)
   {
	   
	   switch(name)
	   {
	   case "client":
		   ClientBLL clientBLL = new ClientBLL();
           List<Client> clientsList = clientBLL.clientDAO.report();
           
           for(Client client : clientsList)
           {
               table.addCell(Integer.toString(client.getIdClient()));
               table.addCell(client.getFirstName());
               table.addCell(client.getLastName());
               table.addCell(client.getAdress());
           }
	   break;
	   
	   case "product":
		   ProductBLL productBLL = new ProductBLL();
           List<Product> productsList = productBLL.productDAO.report();
           
           for(Product product : productsList)
           {
               table.addCell(Integer.toString(product.getIdProduct()));
               table.addCell(product.getName());
               table.addCell(Double.toString(product.getPrice()));
               table.addCell(Integer.toString(product.getQuantity()));
               
           }
	   break;
	   
	   case "order":
		   OrderBLL orderBLL = new OrderBLL();
           List<Order> ordersList = orderBLL.orderDAO.report();
           
           for(Order order : ordersList)
           {
               table.addCell(Integer.toString(order.getIdOrder()));
               table.addCell(Integer.toString(order.getClientID()));
               table.addCell(Integer.toString(order.getProductID()));
               table.addCell(Integer.toString(order.getQuantity()));
           }
	   break;
	   }
       
   }
   
   /**
    * Method for generating the table, using the previously implemented addHeader and addRows methods
    * @param name name represents the name of the table from the DataBase for which we will construct a table in the PDF
    */
   public void generateTable(String name)
   {
       try {
           PdfWriter.getInstance(document, new FileOutputStream(name.toUpperCase()  + ".pdf"));
           document.open();
           PdfPTable newTable = null;
           
           switch(name)
           {
           case "client":
        	   newTable = new PdfPTable(4);
    	   break;
    	   
           case "product":
        	   newTable = new PdfPTable(4);
    	   break;
    	   
           case "order":
        	   newTable = new PdfPTable(4);
    	   break;
           }
          
           addTableHeader(newTable, name);
           addRows(newTable, name);
           newTable.setSpacingBefore(10);
 
           document.add(newTable);
           document.close();
       } 
       catch (FileNotFoundException | DocumentException e) 
       {
           e.printStackTrace();
       } 
   }
   
   /**
    * Method for generating an OrderBill, which contains the order and its total price
    * @param order represents the order from the DataBase for which the OrderBill is generating
    */
   public void generateOrderBill(Order order, int counter)
   {
       ClientBLL clientBLL = new ClientBLL();
       OrderBLL orderBLL = new OrderBLL();
       ProductBLL productBLL = new ProductBLL();
       try {
           PdfWriter.getInstance(document, new FileOutputStream( "Bill"  + counter + ".pdf"));
           document.open();
	        
	  
	        Font font = FontFactory.getFont(FontFactory.COURIER, 25, BaseColor.BLACK);
	        Font fontTitle = FontFactory.getFont(FontFactory.COURIER, 40, BaseColor.BLACK);
	        Chunk orderChunk = new Chunk("ORDER ", fontTitle);
	        Chunk clientChunk = new Chunk("clientID : " + clientBLL.findClientByID(order.getClientID()).getIdClient(),font );
	        Chunk clientAddressChunk = new Chunk("clientAdress : " + clientBLL.findClientByID(order.getClientID()).getAdress(), font);
	        Chunk clientNameChunk = new Chunk("clientName : " + clientBLL.findClientByID(order.getClientID()).getFirstName() + " " + clientBLL.findClientByID(order.getClientID()).getLastName(), font);
	        Chunk productNameChunk = new Chunk("productName : " + productBLL.findProductByID(order.getProductID()).getName(), font);
	        Chunk productQuantityChunk = new Chunk("productQuantity : " + order.getQuantity(), font);
	        Chunk productPriceChunk = new Chunk("productPricePerUnit : " + productBLL.findProductByID(order.getProductID()).getPrice(), font);
	        
	        double totalPrice = order.getQuantity() * productBLL.findProductByID(order.getProductID()).getPrice();
	        Chunk totalPriceChunk = new Chunk("totalPrice : " + totalPrice);
	    
	        
	        document.add(orderChunk);
	        document.add(new Phrase("\n"));
	        document.add(clientChunk);
	        document.add(new Phrase("\n"));
	        document.add(clientAddressChunk);
	        document.add(new Phrase("\n"));
	        document.add(clientNameChunk);
	        document.add(new Phrase("\n"));
	        document.add(productNameChunk);
	        document.add(new Phrase("\n"));
	        document.add(productQuantityChunk);
	        document.add(new Phrase("\n"));
	        document.add(productPriceChunk);
	        document.add(new Phrase("\n"));
	        document.add(totalPriceChunk);
	        document.close();
       } catch (DocumentException | FileNotFoundException e)
       {
           e.printStackTrace();
       } 
   }
   
   /**
    * Method for generating a PDF file displaying a certain message, in case the placed order requested a bigger quantity of a certain product
    * than the quantity that is currently in the stock
    * @param message represents the message that will be displayed
    */
   public void notEnoughProducts(String message){
       try {
           PdfWriter.getInstance(document, new FileOutputStream("UnsuccesfullOrder.pdf"));
           document.open();
           
           Font font = FontFactory.getFont(FontFactory.COURIER_BOLD, 15, BaseColor.BLACK);
           Chunk messageChunk = new Chunk(message, font);
          
           document.add(messageChunk);
           document.add(new Phrase("\n"));
           document.close();
       } 
       catch (FileNotFoundException | DocumentException e) 
       {
           e.printStackTrace();
       } 
   }
}
