package presentation;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import dao.ClientDAO;
import dao.ProductDAO;
import objects.Client;
import objects.Order;
import objects.Product;

/**
 * The class which reads information from the file and transforms the information into SQL commands
 * @author mihai
 *
 */
public class FileParser {
	
	/**
	 * Empty constructor for the FileParser Object
	 */
	public FileParser()
	{
		
	}
	
	/**
	 * Method which takes as input a file, reads every line, divides every line based on the type of command that needs to be executed and calls the 
	 * already implemented methods for creating those commands
	 * @param inputFile the name of the txt file that needs to be parsed
	 */
	int i =0;
	public void readFile(String inputFile)
	{
	
		Scanner scanner = null;
		try
		{
			scanner = new Scanner(new FileReader(inputFile));
		} 
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		
	    while(scanner.hasNext())
	    {
	    	String currentLine = scanner.nextLine();
	    	currentLine = currentLine.replace(":", "");
	    	currentLine = currentLine.replace(",","");
	    	
	    	
	    	String[] words = currentLine.split(" ");
	    	
	    	switch(words[0])
	    	{
	    	case "Insert":
	    		switch(words[1])
	    		{
	    		case "client":
	    			ClientBLL client = new ClientBLL();
	    			Client newClient = new Client(words[2],words[3],words[4]);
	    			client.insertClient(newClient);
	    		break;
	    			
	    		case "product":
	    			ProductBLL product = new ProductBLL();
	    			double price = Float.parseFloat(words[4]);
	    			int quantity = Integer.parseInt(words[3]);
	    			Product newProduct = new Product(words[2], price, quantity);
	    			product.insertProduct(newProduct);
	    		break;
	    			
	    		}
	    	
    		break;
	    	
	    	case "Report":
		    	switch(words[1])
		    	{
		    	case "client":
		    		
		    		PDFGenerator generatePDFClient = new PDFGenerator();
					generatePDFClient.generateTable(words[1]);	
	    		break;
	    		
		    	case "product":
		    		
		    		PDFGenerator generatePDFProduct = new PDFGenerator();
					generatePDFProduct.generateTable(words[1]);	
				break;
				
		    	case "order":
		    		
		    		PDFGenerator generatePDFCounter = new PDFGenerator();
		    		generatePDFCounter.generateTable(words[1]);	
	    		break;
		    	}
		    	
    		break;
	    	
	    	case "Delete":
	    		switch(words[1])
	    		{
	    		case "client":
	    			ClientBLL client = new ClientBLL();
	    			Client deletedClient = new Client(words[2], words[3], words[4]);
	    			int idClient = client.getDAO().getClientDAOId(deletedClient);
	    			client.deleteClient(idClient);
	    			
    			break;
    			
	    		case "Product":
	    			ProductBLL product = new ProductBLL();
	    			Product deletedProduct = new Product(words[2]);
	    			int idProduct = product.getDAO().getProductDAOiD(deletedProduct);
	    			
	    			product.deleteProduct(idProduct);
	    		break;
	    		}
		    	
    		break;
	    	
	    	case "Order":
	    		OrderBLL order = new OrderBLL();
	    		
	    		ClientBLL client = new ClientBLL();
	    		Client client1 = new Client(words[1], words[2]);
	    		int idClient = client.getDAO().getClientDAOId(client1);
	    		
	    		ProductBLL product = new ProductBLL();
	    		Product product1 = new Product(words[3]);
	    		int idProduct = product.getDAO().getProductDAOiD(product1);
	    		
	    		int quantity = Integer.parseInt(words[4]);
	    		Order newOrder = new Order(idClient, idProduct, quantity);
	    		order.insertOrder(newOrder,++i);
	    		
	    		
    		break;
	    	
	    	default:
	    		System.out.println("No match");
	    	}
	    }
	}
	

//	public static void main(String args[]) throws FileNotFoundException  
//
//	{
//		FileParser fp = new FileParser();
//		fp.readFile("commands.txt");
//		System.out.println("The parsing of the file ended");
//	}
	
}

