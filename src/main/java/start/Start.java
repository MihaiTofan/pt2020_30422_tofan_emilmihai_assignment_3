package start;
import java.sql.Connection;
import java.util.List;

import dao.*;
import connection.*;
import objects.*;
import presentation.FileParser;

/**
 * The class that starts the execution of the entire program
 * @author mihai
 *
 */
public class Start {

	/**
	 * Main method, executed the program
	 * @param args represents the name of the file given as a command line argument
	 */
	public static void main(String[] args) {
		
		FileParser fp = new FileParser();
		fp.readFile(args[0]);
		System.out.println("The parsing of the file ended");
		
	}

}
