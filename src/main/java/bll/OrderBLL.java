package bll;

import java.io.FileNotFoundException;
import java.util.List;

import dao.ClientDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import objects.Client;
import objects.Order;
import objects.Product;
import presentation.PDFGenerator;

/**
 * Class for executing the logic behind the operations regarding an Order Object
 * @author mihai
 *
 */
public class OrderBLL
{
  public OrderDAO orderDAO;
  
  /**
   * Constructor that initialises an OrderBLL object for the execution of the commands
   */
  public OrderBLL()
	{
		this.orderDAO = new OrderDAO();
	}
  
  /**
   * Inserts an Order in the DataBase. It checks to see if the same order has already been placed, if the client placing the order is still
   * in the database, if the product for which the order is placed exists in the database and if the requested quantity exists in stock.
   * If all these conditions were satisfied, the order will be placed. One single unsatisfied condition is enough for the order not to be placed.
   * Furthermore, if the quantity requested in the order surpasses the quantity in stock, a PDF file will be generated, notifying the client.
   * @param order represents the Order object that will be inserted
   */
  public void insertOrder(Order order, int counter)
  {
	  List<Order> ordersList = this.orderDAO.report();
	  
	  
	  List<Client> clientsList = new ClientDAO().report();
	  List<Product> productsList = new ProductDAO().report();
	  
	  boolean noSuchClient = false;
	  boolean noSuchProduct = false;
	  boolean quantityTooBig = false;
	  boolean quantityDeleted = false;
	  
	  int counterClients = 1;
	  int counterProducts = 1;
	  
	  for(Client client : clientsList)
	  {
		  if(order.getClientID() != client.getIdClient())
		  {
			  counterClients++;
		  }
	  }
	  
	  for(Product product : productsList)
	  {
		  if(order.getProductID() != product.getIdProduct())
		  {
			  counterProducts++;
		  }
	  }
	  
	  
	  
	  for(Product product : productsList)
	  {
		  if(order.getProductID() == product.getIdProduct())
		  {
			 if(order.getQuantity() > product.getQuantity())
			 {
				 quantityTooBig = true;
			 }
			 else
			 {
				quantityDeleted = true;
				ProductBLL newProduct = new ProductBLL();
				newProduct.getDAO().update(product, product.getIdProduct(), -order.getQuantity());
			 }
		  }
	  }
	  
	  if(counterClients == clientsList.size())
	  {
		  noSuchClient = true;
	  }
	  
	  if(counterProducts == productsList.size())
	  {
		  noSuchProduct = true;
	  }

	  
	  if(noSuchClient == true && noSuchProduct == true && quantityTooBig == false && quantityDeleted == true)
	  {
		  this.orderDAO.insert(order);
		  PDFGenerator generatePDFCounter = new PDFGenerator();
  		  generatePDFCounter.generateOrderBill(order,counter);	
  		  
		  
	  }
	  
	  if(quantityTooBig == true)
	  {
		  String message = "We are sorry to inform you that the ordered quantity surpasses our current stock.";
		  					
		  PDFGenerator generatePDFCounter = new PDFGenerator();
  		  generatePDFCounter.notEnoughProducts(message);
	  }

  }
  
  /**
   * Executes the report operation for order
   * @return returns a list containing the results of the command
   */
  public List<Order> reportOrder() {
	  return this.orderDAO.report();
  }
  
  /**
   * Deletes an order from the database based on the Order's ID
   * @param id represents the ID of the order that will be deleted
   */
  public void deleteOrder(int id)
  {
	  List<Order> ordersList = this.orderDAO.report();
	  
	  for(Order order1 : ordersList)
	  {
		  
		  
		  if(order1.getIdOrder() == id)
		  {
			  
			  this.orderDAO.deleteOrder(id);
			  
		  }
	  }
  }
  
  /**
   * Getter for an OrderDAO objectc
   * @return returns the OrderDAO object
   */
  public OrderDAO getDAO() {
	  return this.orderDAO;
  }
  
//  public static void main(String args[]) throws FileNotFoundException  
//	{
//	  
//	  	OrderBLL order = new OrderBLL();
//	  	ClientBLL client = new ClientBLL();
//	  	ProductBLL product = new ProductBLL();
//	  	List<Order> ordersList = order.orderDAO.report();
//	  	
//	  	
//	  	Client client1 = new Client("Ion","Rus", "Ploiesti");
//	  	Product product1 = new Product("Peach", 2.0,10);
//	  	
//	  	//client.insertClient(client1);
//	  	//product.insertProduct(product1);
//	  	
//	  	
//	  	Order order1 = new Order(client.getDAO().getClientDAOId(client1), product.getDAO().getProductDAOiD(product1),10);
//	  	
//	  	//order.insertOrder(order1);
//	  	//System.out.println(ordersList);
//	  	
//	  	//order.deleteOrder(order1.getIdOrder());
//	  	
//	  	
//		  //System.out.println(ordersList);
//	}
}
