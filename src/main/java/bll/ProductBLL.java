package bll;

import java.io.FileNotFoundException;
import java.util.List;

import dao.ProductDAO;
import objects.Client;
import objects.Order;
import objects.Product;
/**
 * Class for executing the logic behind the operations regarding a Product Object
 * @author mihai
 *
 */
public class ProductBLL {

	public ProductDAO productDAO;
	
	/**
	 * Constructor that initialises a ProductDAO object for the execution of the commands
	 */
	public ProductBLL()
	{
		this.productDAO = new ProductDAO();
	}
	
	/**
	 * Inserts a Product into the dataBase. It checks to see if the Product is already in the DataBase.
	 *  If the Product is already in the database, it doesn't
	 * insert the Product again but it updates the current quantity of the existing product by adding the quantity of the Product that
	 * was about to be inserted to it.
	 * . If the Product is not already in the DataBase, it is inserted.
	 * @param product
	 */
	public void insertProduct(Product product)
	{
		int id = 0;
		int newQuantity = 0;
		List<Product> productsList = this.productDAO.report();
		//System.out.println(productsList);
		boolean productExists = false;
		for(Product p1 : productsList)
		{
			if(p1.getName().equals(product.getName()))
			{
				productExists = true;
				id = this.productDAO.getProductDAOiD(p1);
				newQuantity = product.getQuantity();
				this.productDAO.update(p1, id, newQuantity);

			}
		}
		
		if(productExists == false)
		{
			this.productDAO.insert(product);
		}
		
	}
	
	/**
	 * Deletes a product from the DataBase.It also checks to see if any orders were placed for that same product. If such orders exist
	 * they will be deleted too
	 * @param id
	 */
	public void deleteProduct(int id)
	{
		Product product;
		try {
			product = productDAO.findById(id);
			this.productDAO.delete(product, id);
			OrderBLL orderBLL = new OrderBLL();
			for(Order order : orderBLL.reportOrder())
			{
				if(order.getProductID() == id)
				{
					orderBLL.deleteOrder(order.getIdOrder());
				}
			}
		} catch (NoSuchMethodException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Getter for a ProductDAO Object
	 * @return returns the ProductDAO Object
	 */
	public ProductDAO getDAO()
	{
		return this.productDAO;
	}
	
	/**
	 * Method for returning a product object from the database using it's id 
	 * @param id represents the id of the product object 
	 * @return returns the product object
	 */
	public Product findProductByID(int id) {
		try {
			return this.productDAO.findById(id);
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
//	public static void main(String args[]) throws FileNotFoundException  
//
//	{
//		ProductBLL product = new ProductBLL();
//		Product product1 = new Product("Apple",20.5,10);
//		Product product2  = new Product("Peach", 15, 30);
//		Product product3 = new Product("Maioneza",1.5,10);
//		Product product4 = new Product("Mici", 2.0, 5);
//		
//		product.insertProduct(product1);
//		product.insertProduct(product2);
//		
//		int productID = product.getDAO().getProductDAOiD(product1);
//		product.deleteProduct(productID);
//		
//		List<Product> productsList = product.productDAO.report();
//		System.out.println(productsList);
//	}
}
