package bll;

import java.io.FileNotFoundException;
import java.util.List;

import dao.ClientDAO;
import objects.Client;
import objects.Order;
/**
 * Class for executing the logic behind the operations regarding a Client Object
 * @author mihai
 *
 */
public class ClientBLL {

	public ClientDAO clientDAO ;
	
	/**
	 * Constructor with no parameters that initialises a ClientDAO object for the execution of the commands
	 */
	public ClientBLL()
	{
		this.clientDAO = new ClientDAO();
	}
	
	/**
	 * Inserts a client into the dataBase. It checks to see if the client is already in the DataBase. If the client is already in the database, it doesn't
	 * insert the Client again. If the Client is not already in the DataBase, it is inserted.
	 * @param client represents the Client Object that will be inserted into the DataBase
	 */
	public void insertClient(Client client)
	{
		List<Client> clientsList = this.clientDAO.report();
		//System.out.println(clientsList);
		boolean clientExists = false;
		for(Client c1 : clientsList)
		{
			if(client.getFirstName().equals(c1.getFirstName())
					&& client.getLastName().equals(c1.getLastName()) 
						&& client.getAdress().equals(c1.getAdress()))
			{
				clientExists = true;
				
			}
		}
		
		//System.out.println(clientExists);
		if(clientExists == false)
		{
			this.clientDAO.insert(client);
		}
		
	}
	
	/**
	 * Deletes a client from the DataBase. It also checks to see if any orders were placed by the same Client. If such orders exist
	 * they will be deleted too
	 * @param id The ID of the Client that will be deleted
	 */
	public void deleteClient(int id)
	{
		Client client;
		try {
			client = clientDAO.findById(id);
			this.clientDAO.delete(client, id);
			OrderBLL orderBLL = new OrderBLL();
			for(Order order : orderBLL.reportOrder())
			{
				if(order.getClientID() == id) {
					orderBLL.deleteOrder(order.getIdOrder());
				}
			}
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Method for returning a client object from the database using it's id 
	 * @param id represents the id of the client object
	 * @return returns the client object
	 */
	public Client findClientByID(int id) {
		try {
			return this.clientDAO.findById(id);
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * Getter for a ClientDAO object
	 * @return the ClientDAO object
	 */
	public ClientDAO getDAO() {
		return this.clientDAO;
	}
	
//	public static void main(String args[]) throws FileNotFoundException  
//
//	{
//		ClientBLL client = new ClientBLL();
//		
//		Client client1 = new Client("George", "Bidian", "Bucuresti");
//		Client client2 = new Client("Gavris", "Vancea", "MM");
//		Client client3 = new Client("Mesesan", "Tudor", "GHG");
//		
//		client.insertClient(client1);
//		client.insertClient(client2);
//		
//		int idClient = client.getDAO().getClientDAOId(client1);
//		client.deleteClient(idClient);
//		
//		List<Client> clientsList = client.clientDAO.report();
//		System.out.println(clientsList);
//	}
}
