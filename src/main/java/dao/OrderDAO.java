package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import connection.ConnectionFactory;
import objects.Order;
/**
 * Class which extends the AbstractDao class, in which the generic parameter becomes the Order Object
 * @author mihai
 *
 */
public class OrderDAO extends AbstractDAO<Order>
{
	
	private String deleteQuery = "DELETE FROM assignment3.order WHERE idOrder = ?";
	
	/**
	 * Method for executing a DELETE SQL command for an order object, deleting the object which has the given ID
	 * @param id represent the ID of the Order that will be deleted
	 */
	public void deleteOrder(int id)
	{
		
		PreparedStatement st = null;
		Connection connect = ConnectionFactory.getConnection();
		try 
		{
			st = connect.prepareStatement(deleteQuery);
			st.setInt(1, id);	
			st.executeQuery();
	
		}
		catch (SQLException e)
		{
			
			e.printStackTrace();
			
		}
		
		
	}
	
	
	
}
