package dao;

import java.sql.*;

import connection.ConnectionFactory;
import objects.Client;

/**
 * Class which extends the AbstractDao class, in which the generic parameter becomes the client Object
 * @author mihai
 *
 */
public class ClientDAO extends AbstractDAO<Client> {

	private String getIDQuery = "SELECT idClient FROM assignment3.client WHERE firstName = ? AND lastName = ?";
	
	/**
	 * Method for retrieving the ID of a Client from the DataBase using the name of the Client
	 * @param client represents the Client Object from the DataBase
	 * @return returns the ID of the Client Object as an integer
	 */
	public int getClientDAOId(Client client)
	{
		
		PreparedStatement st = null;
		ResultSet result = null;
		Connection connect = ConnectionFactory.getConnection();
		try 
		{
			st = connect.prepareStatement(getIDQuery);
			st.setString(1, client.getFirstName());
			st.setString(2,client.getLastName());
				
			result = st.executeQuery();
			result.next();
		    int id = result.getInt(1);
		    return id;
			
		}
		catch (SQLException e)
		{
			
			e.printStackTrace();
			return 0;
		}
	}
	
	
}
