package dao;

import java.sql.*;

import connection.ConnectionFactory;
import objects.Client;
import objects.Product;

/**
 * Class which extends the AbstractDao class, in which the generic parameter becomes the Product Object
 * @author mihai
 *
 */
public class ProductDAO extends AbstractDAO<Product>
{

	 private String updateQuery = "UPDATE assignment3.product SET quantity=? WHERE idProduct=?";
	 private String getIDQuery = "SELECT idProduct FROM assignment3.product WHERE name = ?";
	 
	 /**
	  *  Method for retrieving the ID of a Product from the DataBase
	  * @param product represents the Product Object from the DataBase
	  * @return returns the ID of the Product as an int
	  */
	 public int getProductDAOiD(Product product)
		{
			
			PreparedStatement st = null;
			ResultSet result = null;
			Connection connect = ConnectionFactory.getConnection();
			try 
			{
				st = connect.prepareStatement(getIDQuery);
				st.setString(1, product.getName());
				result = st.executeQuery();
				result.next();
			    int id = result.getInt(1);
			    return id;
				
			}
			catch (SQLException e)
			{
				
				e.printStackTrace();
				return 0;
			}
			
			
		}
	 
	 /**
	  * Updates the quantity attribute of an Product Object
	  * @param p1 represents the Product Object that will be updated
	  * @param id represents the Product's ID
	  * @param addedQuantity represents the quantity that will be added to the current quantity of the object
	  */
	 public void update(Product p1, int id, int addedQuantity)
	 {
		 p1.setIdProduct(id);
		 PreparedStatement st = null;
		 int newQuantity = p1.getQuantity() + addedQuantity;
		 		 Connection connect = ConnectionFactory.getConnection();
		 try
		 {
			st = connect.prepareStatement(updateQuery);
			st.setInt(1, newQuantity );
			st.setInt(2, id);
			st.executeUpdate();
			p1.setQuantity(newQuantity);
		 } 
		 catch (SQLException e)
		 {
			e.printStackTrace();
		 }
		 
	 }
}
